# tctl - Terminal Control

Package provides abstractions over xterm control sequences for use in simple console applications

# go doc output #
```go
package tctl // import "gitlab.com/mpizka/tctl"


CONSTANTS

const (
	ESC = "\033"    // escape
	CSI = ESC + "[" // control-sequence-intro

	CLEAR    = CSI + "2J"
	HIDE_CUR = CSI + "?25l"
	SHOW_CUR = CSI + "?25h"
	MOVE_CUR = CSI + "%d;%dH"
	SAVE_CUR = CSI + "s"
	LOAD_CUR = CSI + "u"
	// print ctrl
	NORMAL  = CSI + "0m"
	BOLD    = CSI + "1m"
	REVERSE = CSI + "7m"
	// default foreground colors
	FBLACK   = CSI + "30m"
	FRED     = CSI + "31m"
	FGREEN   = CSI + "32m"
	FBROWN   = CSI + "33m"
	FBLUE    = CSI + "34m"
	FMAGENTA = CSI + "35m"
	FCYAN    = CSI + "36m"
	FWHITE   = CSI + "37m"
	// default background colors
	BBLACK   = CSI + "40m"
	BRED     = CSI + "41m"
	BGREEN   = CSI + "42m"
	BBROWN   = CSI + "43m"
	BBLUE    = CSI + "44m"
	BMAGENTA = CSI + "45m"
	BCYAN    = CSI + "46m"
	BWHITE   = CSI + "47m"
	// 256 color terminal
	FCOL256 = CSI + "38;5;%dm"
	BCOL256 = CSI + "48;5;%dm"
)

VARIABLES

var Screen *bytes.Buffer = bytes.NewBuffer([]byte{})
    Screen buffer for manipulation before drawing


FUNCTIONS

func BCol256(c uint8) string
    BCol256 returns sequence for background 256color c (0-255)

func Clear()
    Clear clears the terminal and sets the cursor to the topleft position

func Draw()
    Draw draws the screenbuffer to the terminal
    Terminal is cleared before drawing
    Screenbuffer is emptied after drawing

func FCol256(c uint8) string
    FCol256 returns sequence for foreground 256color c (0-255)

func Getsize() (w, h int)
    Getsize returns terminals width / height

func Goxy(x, y int)
    Goxy moves the cursor to the specified position

func HideCur()
    HideCur hides the cursor

func LoadCur()
    LoadCur restores cursor to stored pos (SaveCur())

func Raw()
    Raw puts the terminal into Raw-Mode
    (defer Restore() after this)

func Restore()
    Restore restores the terminal to its stored state
    Should be defered after Raw()

func SaveCur()
    SaveCur saves cursor position

func Sclear()
    Sclear empties the screen buffer

func Sgoxy(x, y int)
    Sgoxy writes a move command (see Goxy) to the screenbuffer

func ShowCur()
    ShowCur shows the cursor

```
