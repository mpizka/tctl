// Package tctl (terminal ctl) provides abstractions to the xterm
// control sequences for use in simple terminal applications
//
// Info about terminal control sequences can be found in the manpages:
// `man console_codes`

package tctl

import (
    "bytes"
    "fmt"
    "golang.org/x/crypto/ssh/terminal"
)


const (
    ESC      = "\033"            // escape
    CSI      = ESC+"["           // control-sequence-intro

    CLEAR    = CSI+"2J"
    HIDE_CUR = CSI+"?25l"
    SHOW_CUR = CSI+"?25h"
    MOVE_CUR = CSI+"%d;%dH"
    SAVE_CUR = CSI+"s"
    LOAD_CUR = CSI+"u"
    // print ctrl
    NORMAL   = CSI+"0m"
    BOLD     = CSI+"1m"
    REVERSE  = CSI+"7m"
    // default foreground colors
    FBLACK   = CSI+"30m"
    FRED     = CSI+"31m"
    FGREEN   = CSI+"32m"
    FBROWN   = CSI+"33m"
    FBLUE    = CSI+"34m"
    FMAGENTA = CSI+"35m"
    FCYAN    = CSI+"36m"
    FWHITE   = CSI+"37m"
    // default background colors
    BBLACK   = CSI+"40m"
    BRED     = CSI+"41m"
    BGREEN   = CSI+"42m"
    BBROWN   = CSI+"43m"
    BBLUE    = CSI+"44m"
    BMAGENTA = CSI+"45m"
    BCYAN    = CSI+"46m"
    BWHITE   = CSI+"47m"
    // 256 color terminal
    FCOL256   = CSI+"38;5;%dm"
    BCOL256   = CSI+"48;5;%dm"

)


// Getsize returns terminals width / height
func Getsize() (w,h int) {
    w,h,_ = terminal.GetSize(0)
    return
}

// pgf global store for state changing functions
var oldState *terminal.State

// Raw puts the terminal into Raw-Mode
// defer Restore after this
func Raw() {
    state , err := terminal.MakeRaw(0)
    if err != nil {
        panic(err)
    }
    oldState = state
}

// Restore restores the terminal to its stored state
// Primarily this is defered after Raw()
func Restore() {
    if oldState == nil {
        panic("no state stored")
    }
    terminal.Restore(0, oldState)
}


// ShowCur shows the cursor
func ShowCur() {
    fmt.Printf(SHOW_CUR)
}

// HideCur hides the cursor
func HideCur() {
    fmt.Printf(HIDE_CUR)
}


// Screen buffer for manipulation before drawing
var Screen *bytes.Buffer = bytes.NewBuffer([]byte{})


// Draw draws the screenbuffer to the terminal
// Terminal is cleared before drawing
// Screenbuffer is emptied after drawing
func Draw() {
    Clear()
    fmt.Printf(Screen.String())
    Screen.Reset()
}


// Sclear empties the screen buffer
func Sclear() {
    Screen.Reset()
}


// Clear clears the terminal and sets the cursor to the topleft position
func Clear() {
    fmt.Printf(CLEAR)
    Goxy(1,1)
}


// Sgoxy writes a move command (see Goxy) to the screenbuffer
func Sgoxy(x, y int) {
    fmt.Fprintf(Screen, MOVE_CUR, y, x)
}


// Goxy moves the cursor to the specified position
func Goxy(x, y int) {
    fmt.Printf(MOVE_CUR, y, x)
}


// LoadCur restores cursor to stored pos (SaveCur())
func LoadCur()() {
    fmt.Printf(LOAD_CUR)
}


// SaveCur saves cursor position
func SaveCur()() {
    fmt.Printf(SAVE_CUR)

}

// FCol256 returns sequence for foreground 256color c (0-255)
func FCol256(c uint8) string {
    return fmt.Sprintf(FCOL256, c)
}
// BCol256 returns sequence for background 256color c (0-255)
func BCol256(c uint8) string {
    return fmt.Sprintf(BCOL256, c)
}
